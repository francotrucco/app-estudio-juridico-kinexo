using System;
using Microsoft.Practices.Unity;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Services;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using App_Estudio_Juridico_Kinexo.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using App_Estudio_Juridico_Kinexo.Controllers;
using Microsoft.Owin.Security;

namespace App_Estudio_Juridico_Kinexo.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IAccountant, AccountantServices>();
            container.RegisterType<ILawyer, LawyersServices>();
            container.RegisterType<INews, NewsServices>();
            container.RegisterType<IHumanResourceManager, HumanResourceManagerServices>();
            container.RegisterType<ISecretary, SecretaryServices>();
            container.RegisterType<IProcedure, ProcedureServices>();
            container.RegisterType<IPeople, PeopleServices>();
            container.RegisterType<IDocumentTypeNumber, DocumentTypeServices>();
            container.RegisterType<IGender, GenderServices>();
            container.RegisterType<IClient, ClientServices>();
            container.RegisterType<INotifications, NotificationServices>();
            container.RegisterType<IProcedureStates, ProcedureStateServices>();
            container.RegisterType<ISpecialties, SpecialtiesServices>();
            

            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<AccountController>(new InjectionConstructor());
        }
    }
}
