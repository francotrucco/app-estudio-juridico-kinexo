﻿using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class HomeController : Controller
    {
        private INotifications _notifications;
        public HomeController(INotifications Notifications)
        {
            _notifications = Notifications;
        }
        public ActionResult Index()
        {
            if (User.IsInRole("Lawyer"))
            {
                return View(_notifications.GetAllNotificationsByUsername(WebSecurity.CurrentUserName));
            }
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactModel model)
        {
            try
            {
                return View("Index");
            }
            catch
            {
                return View(model);
            }
            
        }
    }
}