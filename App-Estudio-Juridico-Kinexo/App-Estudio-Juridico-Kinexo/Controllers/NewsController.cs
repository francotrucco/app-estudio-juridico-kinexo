﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Contracts;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class NewsController : Controller
    {
        private INews _news;
        public NewsController(INews News)
        {
            _news = News;
        }
        
        // GET: News
        public ActionResult Index()
        {
            return View(_news.GetNews());
        }

        [Authorize(Roles = "Admin")]
        // GET: News/Create
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        // POST: News/Create
        [HttpPost]
        public ActionResult Create(NewsModel model)
        {
            model.PublicationDate = DateTime.Today;
            model.NewID = _news.GetNextNewID();
            if (ModelState.IsValid)
            {
                if (_news.Create(model))
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        // GET: News/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_news.GetNewByID(id));
        }

        [Authorize(Roles = "Admin")]
        // POST: News/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, NewsModel model)
        {
            if (ModelState.IsValid)
            {
                if (_news.Edit(id, model))
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        // GET: News/Delete/5
        public ActionResult Delete(int id)
        {
            if (_news.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [Authorize(Roles = "Admin")]
        // POST: News/Delete/5
        [HttpPost]
        public string Delete(int id, NewsModel model)
        {
            return "Se eliminó la noticia nro. " + id.ToString();
        }
    }
}
