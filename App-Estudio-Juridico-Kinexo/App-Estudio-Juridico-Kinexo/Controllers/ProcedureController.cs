﻿using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class ProcedureController : Controller
    {
        private IProcedure _procedure;
        private ILawyer _lawyer;
        private IClient _client;
        public ProcedureController(IProcedure Procedure, ILawyer Lawyer, IClient Client)
        {
            _procedure = Procedure;
            _lawyer = Lawyer;
            _client = Client;
        }
        //[Authorize(Roles = "Lawyers")]
        // GET: Procedure/Create
        public ActionResult Create()
        {
            var model = new ProcedureModel();
            //// PRUEBA
            LawyerModel lawyer = _lawyer.GetLawyerByID(1); //Este ID sale del abogado logeado actualmente
            model = new ProcedureModel
            {
                LawyerID = lawyer.LawyerID,
                LawyerInCharge = lawyer.FullName,
                StartingDate = DateTime.Today,
                ProcedureStateID = 1
            };
            ViewBag.ClientList = new SelectList(_client.GetAllClients(), "ClientID", "FullName", _client.GetClientByID(1));
            ////
            return View(model);
        }

        // POST: Procedure/Create
        [HttpPost]
        public ActionResult Create(ProcedureModel model)
        {
            try
            {
                ViewBag.ClientList = new SelectList(_client.GetAllClients(), "ClientID", "FullName", _client.GetClientByID(1));
                if (ModelState.IsValid)
                {

                    if (_procedure.Create(model))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    return View(model);
                }
                return View(model);
            }
            catch (NotImplementedException e)
            {
                return View(model);
            }
        }

        // GET: Procedure/Edit/5
        public ActionResult Edit(int id)
        {
            var model = _procedure.GetProcedureByID(id);
            var user = WebSecurity.CurrentUserName;
            var lawyerInCharge = _lawyer.GetLawyerByUsername(user);
            ViewBag.ClientList = new SelectList(_client.GetAllClients(), "ClientID", "FullName", _client.GetClientByID(1));
            model.LawyerInCharge = lawyerInCharge.FullName;
            model.LawyerID = lawyerInCharge.LawyerID;
            return View(model);
        }

        // POST: Procedure/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProcedureModel model)
        {
            try
            {
                ViewBag.ClientList = new SelectList(_client.GetAllClients(), "ClientID", "FullName", _client.GetClientByID(1));
                if (ModelState.IsValid)
                {
                    if (_procedure.Edit(id, model))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    return View(model);
                }
                return View(model);
            }
            catch
            {
                return View();
            }
        }
    }
}
