﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Contracts;
using WebMatrix.WebData;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class LawyerController : Controller
    {
        private ILawyer _lawyer;
        private IPeople _person;
        private IDocumentTypeNumber _documents;
        private IGender _genders;
        private ISpecialties _specialties;
        public LawyerController(ILawyer Lawyer, IPeople People, IDocumentTypeNumber Documents, IGender Gender, ISpecialties Specialties)
        {
            _lawyer = Lawyer;
            _person = People;
            _documents = Documents;
            _genders = Gender;
            _specialties = Specialties;
        }
        // GET: Lawyer
        public ActionResult Index()
        {
            IList<LawyerModel> allLawyers = _lawyer.GetLawyers();
            if (allLawyers != null)
            {
                return View();
            }
            return View(allLawyers);
        }

        // GET: Lawyer/Create
        public ActionResult Create()
        {
            ViewBag.DocumentTypes = new SelectList(_documents.GetDocumentNumberTypes(), "DocumentNumberTypeID", "Abbreviation", _documents.GetFirstDocumentNumberType());
            ViewBag.Genders = new SelectList(_genders.GetAllGenders(), "GenderID", "Gender", _genders.GetFirstGender());
            ViewBag.Specialties = new SelectList(_specialties.GetAllSpecialties(), "LawyerSpecialtyID", "Description", _specialties.GetAllSpecialties().FirstOrDefault());
            return View();
        }

        // POST: Lawyer/Create
        [HttpPost]
        public ActionResult Create(LawyerModel model)
        {
            ViewBag.DocumentTypes = new SelectList(_documents.GetDocumentNumberTypes(), "DocumentNumberTypeID", "Abbreviation", _documents.GetFirstDocumentNumberType());
            ViewBag.Genders = new SelectList(_genders.GetAllGenders(), "GenderID", "Gender", _genders.GetFirstGender());
            if (ModelState.IsValid)
            {
                if (_lawyer.Create(model))
                {
                    return RedirectToAction("Index", "Home");
                }
                return View(model);
            }
            return View(model);
        }

        // GET: Lawyer/Edit
        public ActionResult Edit(int id)
        {
            ViewBag.DocumentTypes = new SelectList(_documents.GetDocumentNumberTypes(), "DocumentNumberTypeID", "Abbreviation", _documents.GetFirstDocumentNumberType());
            ViewBag.Genders = new SelectList(_genders.GetAllGenders(), "GenderID", "Gender", _genders.GetFirstGender());
            LawyerModel model = _lawyer.GetLawyerByUsername(WebSecurity.CurrentUserName);
            return View(model);
        }

        // POST: Lawyer/Edit
        [HttpPost]
        public ActionResult Edit(int id, NewsModel model)
        {
            ViewBag.DocumentTypes = new SelectList(_documents.GetDocumentNumberTypes(), "DocumentNumberTypeID", "Abbreviation", _documents.GetFirstDocumentNumberType());
            ViewBag.Genders = new SelectList(_genders.GetAllGenders(), "GenderID", "Gender", _genders.GetFirstGender());

            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
    }
}
