﻿using App_Estudio_Juridico_Kinexo.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    [Authorize(Roles = "Lawyer")]
    public class NotificationController : Controller
    {
        private INotifications _notifications;
        public NotificationController(INotifications Notifications)
        {
            _notifications = Notifications;
        }
        // GET: Notification
        public ActionResult Index()
        {
            if (User.IsInRole("Lawyer"))
            {
                return View(_notifications.GetAllNotificationsByUsername(WebSecurity.CurrentUserName));
            }
            return View();
        }

        // GET: Notification/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        ////// GET: Notification/Create
        ////public ActionResult Create()
        ////{
        ////    return View();
        ////}

        ////// POST: Notification/Create
        ////[HttpPost]
        ////public ActionResult Create(FormCollection collection)
        ////{
        ////    try
        ////    {
        ////        // TODO: Add insert logic here

        ////        return RedirectToAction("Index");
        ////    }
        ////    catch
        ////    {
        ////        return View();
        ////    }
        ////}

        // GET: Notification/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Notification/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        ////// GET: Notification/Delete/5
        ////public ActionResult Delete(int id)
        ////{
        ////    return View();
        ////}

        ////// POST: Notification/Delete/5
        ////[HttpPost]
        ////public ActionResult Delete(int id, FormCollection collection)
        ////{
        ////    try
        ////    {
        ////        // TODO: Add delete logic here

        ////        return RedirectToAction("Index");
        ////    }
        ////    catch
        ////    {
        ////        return View();
        ////    }
        ////}
    }
}
