﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Contracts;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class HumanResourceManagerController : Controller
    {
        private IHumanResourceManager _hrm;

        public HumanResourceManagerController(IHumanResourceManager HRM)
        {
            _hrm = HRM;
        }
        // GET: HumanResourceManager/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HumanResourceManager/Create
        [HttpPost]
        public ActionResult Create(HumanResourceManagerModel model)
        {
            if (ModelState.IsValid)
            {
                if (_hrm.Create(model))
                {
                    return RedirectToAction("Index", "Home");
                }
                return View(model);
            }
            return View(model);
        }

        // GET: HumanResourceManager/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HumanResourceManager/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HumanResourceManagerModel model)
        {
            if (ModelState.IsValid)
            {
                if (_hrm.Edit(id, model))
                {
                    return RedirectToAction("Index", "Home");
                }
                return View(model);
            }
            return View(model);
        }
    }
}
