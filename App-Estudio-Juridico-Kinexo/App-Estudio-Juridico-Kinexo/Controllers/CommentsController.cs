﻿using App_Estudio_Juridico_Kinexo.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class CommentsController : Controller
    {
        private IComments _comments;
        public CommentsController(IComments Comments)
        {
            _comments = Comments;
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        //public ActionResult Create()
        //{
        //    try
        //    {

        //    }
        //    catch
        //    {
        //        return View(model);
        //    }
        //}
        public ActionResult Reply(int id)
        {
            return View(_comments.GetCommentById(id));
        }
    }
}
