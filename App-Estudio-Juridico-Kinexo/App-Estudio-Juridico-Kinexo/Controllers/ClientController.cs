﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using WebMatrix.WebData;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    [Authorize(Roles = "Clients, Admins")]
    public class ClientController : Controller
    {
        private IClient _client;

        public ClientController(IClient Client)
        {
            _client = Client;
        }
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }

        // GET: Client/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult Create(ClientModel model)
        {
            try
            {
                if (_client.Create(model))
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_client.GetClientByID(id));
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ClientModel model)
        {
            try
            {
                if (_client.Edit(id, model))
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ClientModel model)
        {
            try
            {
                if (_client.Delete(id))
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult MyCases()
        {
            return View(_client.GetAllCases(WebSecurity.CurrentUserName));
        }
    }
}
