﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;


namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class AccountantController : Controller
    {
        private IAccountant _accountant;
        private IPeople _person;
        private IDocumentTypeNumber _documents;
        private IGender _genders;
        public AccountantController(IAccountant Accountant, IPeople People, IDocumentTypeNumber Documents, IGender Gender)
        {
            _accountant = Accountant;
            _person = People;
            _documents = Documents;
            _genders = Gender;

        }
        // GET: Accountant
        public ActionResult Index()
        {
            return View();
        }

        // GET: Accountant/Create
        public ActionResult Create()
        {
            ViewBag.DocumentTypes = new SelectList(_documents.GetDocumentNumberTypes(), "DocumentNumberTypeID", "Abbreviation", _documents.GetFirstDocumentNumberType());
            ViewBag.Genders = new SelectList(_genders.GetAllGenders(), "GenderID", "Gender", _genders.GetFirstGender());
            return View();
        }

        // POST: Accountant/Create
        [HttpPost]
        public ActionResult Create(AccountantModel model)
        {
            model.AccountantID = _accountant.GetNextAccountantID();
            model.PersonID = _person.GetNextPeopleId();
            if (ModelState.IsValid)
            {
                if (_accountant.Create(model))
                {
                    return View("../Shared/EmployeesIndex");
                }
                else
                {
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: Accountant/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.DocumentTypes = new SelectList(_documents.GetDocumentNumberTypes(), "DocumentNumberTypeID", "Abbreviation", _documents.GetFirstDocumentNumberType());
            ViewBag.Genders = new SelectList(_genders.GetAllGenders(), "GenderID", "Gender", _genders.GetFirstGender());
            AccountantModel model = _accountant.GetAccountantByID(id);
            return View(model);
        }

        // POST: Accountant/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AccountantModel model)
        {
            if (ModelState.IsValid)
            {
                if (_accountant.Edit(id, model))
                {
                    return View("Index");
                }
                return View(model);
            }
            return View(model);
        }
    }
}