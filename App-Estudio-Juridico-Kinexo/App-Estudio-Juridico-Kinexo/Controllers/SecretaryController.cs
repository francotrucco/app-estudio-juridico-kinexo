﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Controllers
{
    public class SecretaryController : Controller
    {
        // GET: Secretary
        public ActionResult Index()
        {
            return View();
        }
        
        // GET: Secretary/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Secretary/Create
        [HttpPost]
        public ActionResult Create(SecretaryModel model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        // GET: Secretary/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Secretary/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SecretaryModel model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
    }
}
