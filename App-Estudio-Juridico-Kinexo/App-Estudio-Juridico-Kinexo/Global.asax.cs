﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebMatrix.WebData;

namespace App_Estudio_Juridico_Kinexo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            WebSecurity.InitializeDatabaseConnection("EstudioConnection", "UserProfile", "UserdID", "UserName", true);

            App_Start.UnityWebActivator.Start();
        }
    }
}
