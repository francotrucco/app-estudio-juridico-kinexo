﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(App_Estudio_Juridico_Kinexo.Startup))]
namespace App_Estudio_Juridico_Kinexo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
