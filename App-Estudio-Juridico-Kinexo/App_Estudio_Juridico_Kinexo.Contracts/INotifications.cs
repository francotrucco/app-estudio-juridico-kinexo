﻿using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface INotifications
    {
        bool SendNotificationToLawyer(int id);
        int GetNextNotificationId();
        IList<NotificationModel> GetAllNotificationsByLawyerID(int id);
        IList<NotificationModel> GetAllNotificationsByUsername(string usernam);
    }
}
