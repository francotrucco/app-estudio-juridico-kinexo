﻿using App_Estudio_Juridico_Kinexo.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IGender
    {
        IList<GenderModel> GetAllGenders();
        GenderModel GetFirstGender();
    }
}
