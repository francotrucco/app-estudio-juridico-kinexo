﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface INews
    {
        bool Create(NewsModel news);
        bool Edit(int id, NewsModel news);
        bool Delete(int id);
        IList<NewsModel> GetNews();
        NewsModel GetNewByID(int id);
        int GetNextNewID();
    }
}
