﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IProcedureStates
    {
        int GetStartingProcedureState();
    }
}
