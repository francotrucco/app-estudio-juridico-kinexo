﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class HumanResourceManagerModel : PersonModel
    {
        [Display(Name = "ID del Administrador de Recursos Humanos")]
        public int HumanResourceManagerID { get; set; }
    }
}