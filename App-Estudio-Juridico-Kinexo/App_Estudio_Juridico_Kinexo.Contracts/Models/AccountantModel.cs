﻿using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class AccountantModel : PersonModel
    {
        [Display(Name = "ID de Contador")]
        public int AccountantID { get; set; }
    }
}