﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class ProcedureStateModel
    {
        public int ProcedureStateID { get; set; }

        public string ProcedureStateDescription { get; set; }
        
    }
}