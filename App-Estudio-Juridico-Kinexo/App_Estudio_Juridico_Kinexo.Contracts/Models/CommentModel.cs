﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class CommentModel
    {
        public int CommentID { get; set; }
        public string Comment { get; set; }
        public int ProcedureID { get; set; }
    }
}
