﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class SpecialtyModel
    {
        public int LawyerSpecialtyID { get; set; }

        public string Description { get; set; }
    }
}