﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class ContactModel
    {
        [Required(ErrorMessage = "Debe ingresar su Nombre y Apellido")]
        [Display(Name = "Nombre y Apellido")]
        public string FirstAndLastName { get; set; }
        [Display(Name = "Ciudad")]
        public string City { get; set; }
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+", ErrorMessage = "El email ingresado no es válido.")]
        [Display(Name = "E-Mail")]
        [Required(ErrorMessage = "Debe ingresar un e-mail")]
        public string Email { get; set; }
        [Display(Name = "Mensaje")]
        public string Message { get; set; }

    }
}
