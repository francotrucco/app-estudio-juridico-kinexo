﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class CityModel
    {
        [Required(ErrorMessage = "El Código Postal es requerido.")]
        public int PostalCode { get; set; }
        
        public string Name { get; set; }

        public int ProvinceID { get; set; }
    }
}