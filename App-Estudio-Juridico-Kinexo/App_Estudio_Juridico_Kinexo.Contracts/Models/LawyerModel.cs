﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class LawyerModel : PersonModel
    {   
        [Display(Name = "ID de Abogado")]
        public int LawyerID { get; set; }
        [Required (ErrorMessage = "La especialidad es un campo requerido.")]
        [Display(Name = "Especialidad")]
        public int SpecialtyID { get; set; }
    }
}