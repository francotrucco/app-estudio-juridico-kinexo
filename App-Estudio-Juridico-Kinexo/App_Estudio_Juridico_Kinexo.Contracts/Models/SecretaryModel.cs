﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class SecretaryModel : PersonModel
    {
        [Display(Name = "ID de Secretario/a")]
        public int SecretaryID { get; set; }
    }
}