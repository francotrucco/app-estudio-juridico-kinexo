﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class NotificationModel
    {
        public int NotificationID { get; set; }
        public int CommentID { get; set; }
        [Display(Name = "Visto")]
        public bool Seen { get; set; }
        public int LawyerID { get; set; }
        [Display(Name = "Comentario")]
        public string Comment { get; set; }
        [Display(Name = "Cliente")]
        public string Client { get; set; }

    }
}