﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class DocumentTypeNumberModel
    {
        public int DocumentNumberTypeID { get; set; }

        public string DocumentType { get; set; }

        public string Abbreviation { get; set; }
    }
}