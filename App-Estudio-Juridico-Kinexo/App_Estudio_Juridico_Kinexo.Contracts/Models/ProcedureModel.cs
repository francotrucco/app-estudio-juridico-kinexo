﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App_Estudio_Juridico_Kinexo.Models;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class ProcedureModel
    {
        //ID del Caso
        [Display(Name = "Caso Nº")]
        public int ProcedureID { get; set; }
        //ID del Abogado a cargo del caso
        public int LawyerID { get; set; }
        [Display(Name = "Abogado a cargo")]
        public string LawyerInCharge { get; set; }
        [Display(Name = "Cliente")]
        public string Client { get; set; }
        //ID del Estado del caso
        public int ProcedureStateID { get; set; }
        [Display(Name = "Comentarios")]
        public IEnumerable<CommentModel> Comments { get; set; }
        public int CommentID { get; set; }
        public int ClientID { get; set; }
        [Display(Name = "Fecha de inicio")]
        public DateTime StartingDate { get; set; }
        [Display(Name = "Fecha de fin")]
        public DateTime FinishingDate { get; set; }

    }
}