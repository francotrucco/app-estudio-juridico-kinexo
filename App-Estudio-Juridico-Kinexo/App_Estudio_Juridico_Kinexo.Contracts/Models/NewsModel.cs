﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class NewsModel
    {
        [Required(ErrorMessage = "La noticia debe tener un identificador.")]
        public int NewID { get; set; }

        [Required(ErrorMessage = "La noticia debe de tener un título.")]
        [StringLength(200, ErrorMessage = "El título de la noticia pasó los 200 caracteres.")]
        [Display(Name = "Título")]
        public string Head { get; set; }

        [Required(ErrorMessage = "El cuerpo de la Noticia no puede estar vacío.")]
        [StringLength(3000, ErrorMessage = "La noticia sobrepasó la cantidad de caracteres.")]
        [Display(Name = "Cuerpo")]
        public string Body { get; set; }

        [Required(ErrorMessage = "Debe ingresar el autor de la Noticia.")]
        [StringLength(160, ErrorMessage = "El nombre del Autor es muy largo, sobrepasó los 160 caracteres permitidos.")]
        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Debe ingresar la fuente de la Noticia.")]
        [Display(Name = "Fuentes")]
        public string Fonts { get; set; }
        //TODO: Crearlo como un Ilist

        [Required(ErrorMessage = "Debe ingresar la fecha de publicación.")]
        [Display(Name = "Fecha de Publicación")]
        public DateTime PublicationDate { get; set; }

        [Required(ErrorMessage = "Debe ingresar una URL con una imagen para la noticia.")]
        [Display(Name = "URL de Imágen")]
        public string ImageURL { get; set; }

    }
}