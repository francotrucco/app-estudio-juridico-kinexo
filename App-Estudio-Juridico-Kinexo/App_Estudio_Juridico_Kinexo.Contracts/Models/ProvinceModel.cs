﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class ProvinceModel
    {
        [Required(ErrorMessage = "El número de la Provincia es requerido.")]
        public int ProvinceID { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }
    }
}