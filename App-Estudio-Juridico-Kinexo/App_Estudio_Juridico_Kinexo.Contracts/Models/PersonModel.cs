﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace App_Estudio_Juridico_Kinexo.Models
{
    public class PersonModel
    {
        [Required]
        public int PersonID { get; set; }
        [Required(ErrorMessage = "Debe ingresar un nombre y apellido.")]
        [StringLength(120, ErrorMessage = "El nombre y apellido ingresado es muy largo, el límite es 120 caracteres.")]
        [Display(Name = "Nombre y Apellido")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "Debe ingresar su Número de Documento.")]
        [Range(1000000, 99999999)]
        [Display(Name = "Documento")]
        public int DocumentNumber { get; set; }
        [Required(ErrorMessage = "Debe ingresar su Tipo de Documento.")]
        [Range(1, 3)] //TODO: En realidad este Rango debería ser dinámico, depende de la cantidad de entradas que están registradas en la base de datos.
        [Display(Name = "Tipo Doc")]
        public int DocumentNumberTypeID { get; set; }
        [EmailAddress(ErrorMessage = "El email ingresado no es válido.")]
        //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+", ErrorMessage = "El email ingresado no es válido.")]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }
        [Compare("Email", ErrorMessage = "Los emails ingresados no son iguales.")]
        [Display(Name = "Confirmación")]
        public string EmailConfirm { get; set; }
        [Display(Name = "Nacimiento")]
        public DateTime Birthday { get; set; }
        [Display(Name = "CodPostal")]
        public int LocationID { get; set; }
        [Display(Name = "Fecha de Entrada")]
        public DateTime EntranceDate { get; set; }
        [Display(Name = "Género")]
        public int GenderID { get; set; }
        [Display(Name = "Imágen")]
        public string ProfileImage { get; set; }
        [Required(ErrorMessage = "Debe ingresar el título.")]
        [Display(Name = "Título")]
        public string Title { get; set; }
    }
}