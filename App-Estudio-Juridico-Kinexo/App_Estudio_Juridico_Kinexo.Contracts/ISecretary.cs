﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface ISecretary
    {
        bool Create(SecretaryModel secretary);
        bool Edit(int id, SecretaryModel secretary);
        bool Delete(int id);
        IList<SecretaryModel> GetSecretaries();
        SecretaryModel GetSecretaryByID(int id);
        int GetNextSecretaryID();
    }
}
