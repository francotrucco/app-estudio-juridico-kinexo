﻿using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IDocumentTypeNumber
    {
        IList<DocumentTypeNumberModel> GetDocumentNumberTypes();
        DocumentTypeNumberModel GetFirstDocumentNumberType();
    }
}
