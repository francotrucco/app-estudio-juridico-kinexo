﻿using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IClient
    {
        bool Create(ClientModel client);
        bool Edit(int id, ClientModel client);
        bool Delete(int id);
        int GetNextClientID();
        ClientModel GetClientByID(int id);
        IList<ClientModel> GetAllClients();
        bool SendNotificationToLawyer(int id);
        IList<ProcedureModel> GetAllCases(string username);
    }
}
