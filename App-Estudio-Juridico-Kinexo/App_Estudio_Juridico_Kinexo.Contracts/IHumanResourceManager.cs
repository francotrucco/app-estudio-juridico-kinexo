﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IHumanResourceManager
    {
        bool Create(HumanResourceManagerModel HRM);
        bool Edit(int id, HumanResourceManagerModel HRM);
        bool Delete(int id);
        IList<HumanResourceManagerModel> GetHumanResourceManagers();
        HumanResourceManagerModel GetHumanResourceManagerByID(int id);
        int GetNextHumanResourceManagerID();
    }
}
