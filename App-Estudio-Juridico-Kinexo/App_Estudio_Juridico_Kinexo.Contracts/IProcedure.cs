﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IProcedure
    {
        bool Create(ProcedureModel procedure);
        bool Edit(int id, ProcedureModel procedure);
        bool Delete(int id);
        IList<ProcedureModel> GetAllProceduresOfClient(int id);
        ProcedureModel GetProcedureByID(int id);
        int GetNextProcedureID();
        bool GetToNextProcedureState(int id);
        IList<CommentModel> GetAllComentsFromProcedure(int id);
    }
}
