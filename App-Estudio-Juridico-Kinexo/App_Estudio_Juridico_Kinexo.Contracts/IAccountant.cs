﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IAccountant
    {
        /// <summary>
        /// Method that given an Accountant Model creates a new Accountant in the DataBase.
        /// </summary>
        /// <param name="accountant">Accountant model</param>
        /// <returns>True if the creation success. False if the creation fails</returns>
        bool Create(AccountantModel accountant);
        /// <summary>
        /// Method that given an Accountant Model and the Accountant ID edits the information in the DataBase.
        /// </summary>
        /// <param name="id">ID of the Accountant to be modified</param>
        /// <param name="accountant">Accountant Model with the information that will be modified in the DataBase</param>
        /// <returns>True if the edition success. False if the edition fails</returns>
        bool Edit(int id, AccountantModel accountant);
        /// <summary>
        /// Method that given an Accountant ID deletes it from the DataBase
        /// </summary>
        /// <param name="id">ID of the Accountant to be deleted from the DataBase</param>
        /// <returns>True if the delete success. False if the delete fails</returns>
        bool Delete(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <returns>IList of Accountants in the DataBase</returns>
        IList<AccountantModel> GetAccountants();
        /// <summary>
        /// Method that given an Accountant ID returns the Accountant itself.
        /// </summary>
        /// <param name="id">ID of the Accountant to be returned</param>
        /// <returns>Model of the Accountant which ID was given</returns>
        AccountantModel GetAccountantByID(int id);
        /// <summary>
        /// Method that return the next Accountant ID
        /// </summary>
        /// <returns>ID of the Next Accountant</returns>
        int GetNextAccountantID();
    }
}
