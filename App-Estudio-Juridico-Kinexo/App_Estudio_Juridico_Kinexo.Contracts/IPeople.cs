﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IPeople
    {
        int GetNextPeopleId();
    }
}
