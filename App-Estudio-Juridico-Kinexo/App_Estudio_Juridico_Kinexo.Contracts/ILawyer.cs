﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface ILawyer
    {
        bool Create(LawyerModel lawyer);
        bool Edit(int id, LawyerModel lawyer);
        bool Delete(int id);
        IList<LawyerModel> GetLawyers();
        LawyerModel GetLawyerByID(int id);
        int GetNextLawyerID();
        LawyerModel GetLawyerByUsername(string username);
    }
}
