﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;
namespace App_Estudio_Juridico_Kinexo.Contracts
{
    public interface IComments
    {
        /// <summary>
        /// Given an Procedure ID, it creates a new Comment for that Procedure
        /// </summary>
        /// <param name="id">Procedure ID</param>
        /// <returns>If the creation succeed(true) or failed(false)</returns>
        bool CreateNewComment(CommentModel comentario);
        /// <summary>
        /// Method that returns the next ID in the DBB.
        /// </summary>
        /// <returns>The next ID to use.</returns>
        int GetNextCommentId();

        CommentModel GetCommentById(int id);
    }
}
