﻿using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class ProcedureStateServices : IProcedureStates
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public ProcedureStateServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public int GetStartingProcedureState()
        {
            return db.ProcedureStates.Find(1).ProcedureStateID;
        }

    }
}
