﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class LawyersServices : ILawyer
    {
        private App_Estudio_Juridico_KinexoEntities1 db;

        public LawyersServices()
        {
            this.db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(LawyerModel lawyer)
        {
            try
            {

                int id = db.People.Last().PersonID + 1;
                DateTime today = DateTime.Today;
                Person newPerson = new Person
                {
                    PersonID = id,
                    FullName = lawyer.FullName,
                    Title = lawyer.Title,
                    DocumentNumber = lawyer.DocumentNumber,
                    DocumentNumberTypeID = (short)lawyer.DocumentNumberTypeID,
                    Email = lawyer.Email,
                    Birthday = lawyer.Birthday,
                    LocationID = (short)lawyer.LocationID,
                    EntranceDate = today,
                    GenderID = lawyer.GenderID
                };
                db.People.Add(newPerson);
                Lawyer newLawyer = new Lawyer
                {
                    LawyerID = lawyer.LawyerID,
                    PersonID = lawyer.PersonID
                };
                db.Lawyers.Add(newLawyer);
                db.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                db.Lawyers.Remove(db.Lawyers.Find(id));
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(int id, LawyerModel lawyer)
        {
            try
            {
                Lawyer lawyerFound = db.Lawyers.Find(id);
                Person editPerson = db.People.Find(lawyerFound.PersonID);
                editPerson.LocationID = (short)lawyer.LocationID;
                editPerson.Email = lawyer.Email;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public LawyerModel GetLawyerByID(int id)
        {
            try
            {
                Lawyer lawyer = db.Lawyers.Find(id);
                LawyerModel lawyerToReturn = null;
                if (lawyer != null)
                {
                    lawyerToReturn = new LawyerModel
                    {
                        LawyerID = lawyer.LawyerID,
                        PersonID = id,
                        FullName = lawyer.Person.FullName,
                        Title = lawyer.Person.Title,
                        DocumentNumber = lawyer.Person.DocumentNumber,
                        DocumentNumberTypeID = (short)lawyer.Person.DocumentNumberTypeID,
                        Email = lawyer.Person.Email,
                        Birthday = (DateTime)lawyer.Person.Birthday,
                        LocationID = (short)lawyer.Person.LocationID,
                        EntranceDate = (DateTime)lawyer.Person.EntranceDate,
                        GenderID = (int)lawyer.Person.GenderID,
                        ProfileImage = lawyer.Person.ProfileImage
                    };
                }

                return lawyerToReturn;
            }
            catch (NotImplementedException e)
            {
                LawyerModel lawyerNotFound = new LawyerModel
                {
                    LawyerID = -1
                };
                return lawyerNotFound;
            }
        }

        public LawyerModel GetLawyerByUsername(string username)
        {
            try
            {
                var user = db.UserProfiles.First(u => u.UserName == username);
                var person = db.People.First(p => p.Email == user.UserName);
                Lawyer lawyer = db.Lawyers.First(l => l.PersonID == person.PersonID);
                LawyerModel lawyerToReturn = null;
                if (lawyer != null)
                {
                    lawyerToReturn = new LawyerModel
                    {
                        LawyerID = lawyer.LawyerID,
                        PersonID = person.PersonID,
                        FullName = lawyer.Person.FullName,
                        Title = lawyer.Person.Title,
                        DocumentNumber = lawyer.Person.DocumentNumber,
                        DocumentNumberTypeID = (short)lawyer.Person.DocumentNumberTypeID,
                        Email = lawyer.Person.Email,
                        Birthday = (DateTime)lawyer.Person.Birthday,
                        LocationID = (short)lawyer.Person.LocationID,
                        EntranceDate = (DateTime)lawyer.Person.EntranceDate,
                        GenderID = (int)lawyer.Person.GenderID
                    };
                }

                return lawyerToReturn;
            }
            catch (NotImplementedException e)
            {
                LawyerModel lawyerNotFound = new LawyerModel
                {
                    LawyerID = -1
                };
                return lawyerNotFound;
            }
        }

        public IList<LawyerModel> GetLawyers()
        {
            IList<LawyerModel> LawyerListModel = null;
            try
            {
                if (db.Lawyers != null)
                {
                    IList<Lawyer> LawyerList = db.Lawyers.ToList();
                    if (LawyerList.Count > 0)
                    {
                        foreach (var item in LawyerList)
                        {
                            LawyerListModel.Add(new LawyerModel
                            {
                                LawyerID = item.LawyerID,
                                PersonID = item.PersonID,
                                FullName = item.Person.FullName,
                                Email = item.Person.Email,
                                EntranceDate = (DateTime)item.Person.EntranceDate,
                                Birthday = (DateTime)item.Person.Birthday,
                                GenderID = (int)item.Person.GenderID,
                                DocumentNumberTypeID = item.Person.DocumentNumberTypeID,
                                DocumentNumber = item.Person.DocumentNumber,
                                LocationID = item.Person.LocationID,
                                Title = item.Person.Title
                            });
                        }
                    }
                    else
                    {
                        LawyerListModel.Add(new LawyerModel
                        {
                            PersonID = -1
                        });
                    }
                }
                return LawyerListModel;
            }
            catch
            {
                LawyerListModel.Add(new LawyerModel
                {
                    PersonID = -1
                });
                return LawyerListModel;
            }
        }

        public int GetNextLawyerID()
        {
            if (db.Lawyers != null)
            {
                db.Lawyers.SqlQuery("SELECT LawyerID FROM Lawyers ORDER BY LawyerID DESC LIMIT 1");
                var lawyers = db.Lawyers.ToList();
                if (lawyers.Count == 0)
                {
                    return 1;
                }
                return lawyers.Last().LawyerID + 1;
            }
            return 1;
        }
    }
}
