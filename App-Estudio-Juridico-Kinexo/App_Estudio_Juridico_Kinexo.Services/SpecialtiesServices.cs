﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class SpecialtiesServices : ISpecialties
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public SpecialtiesServices()
        {
            this.db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public IList<SpecialtyModel> GetAllSpecialties()
        {
            List<SpecialtyModel> specialtiesList = new List<SpecialtyModel>();
            if (db.LawyerSpecialties != null)
            {
                List<LawyerSpecialty> specialtiesFromDB = db.LawyerSpecialties.ToList();
                foreach (var item in specialtiesFromDB)
                {
                    specialtiesList.Add(new SpecialtyModel {
                        LawyerSpecialtyID = item.LawyerSpecialtyID,
                        Description = item.Description
                    });
                }
            }
            return specialtiesList;
        }
    }
}
