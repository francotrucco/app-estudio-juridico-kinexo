﻿using App_Estudio_Juridico_Kinexo.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class ClientServices : IClient
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public ClientServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(ClientModel client)
        {
            try
            {
                db.People.Add(new Person
                {
                    PersonID = client.PersonID,
                    FullName = client.FullName,
                    Birthday = client.Birthday,
                    DocumentNumber = client.DocumentNumber,
                    DocumentNumberTypeID = (short)client.DocumentNumberTypeID,
                    Email = client.Email,
                    EntranceDate = DateTime.Today,
                    GenderID = client.GenderID,
                    LocationID = (short)client.LocationID,
                    ProfileImage = client.ProfileImage,
                });
                db.Clients.Add(new Client
                {
                    PersonID = client.PersonID,
                    ClientID = GetNextClientID(),
                });
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                ////Si la persona que queremos borrar tiene asignado algún caso entonces no la podemos borrar
                if (db.Procedures.Any(item => item.ClientID == id))
                {
                    return false;
                }
                Client cliente = db.Clients.Find(id);
                db.Clients.Remove(cliente);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(int id, ClientModel client)
        {
            try
            {
                Client editClient = db.Clients.Find(id);
                editClient.ClientID = client.ClientID;
                editClient.PersonID = client.PersonID;
                editClient.Person.LocationID = (short)client.LocationID;
                editClient.Person.FullName = client.FullName;
                editClient.Person.GenderID = client.GenderID;
                editClient.Person.ProfileImage = client.ProfileImage;
                editClient.Person.Birthday = client.Birthday;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IList<ProcedureModel> GetAllCases(string username)
        {
            List<ProcedureModel> casesToReturn = new List<ProcedureModel>();

            if (db.Procedures != null)
            {
                IList<Procedure> procedureList = db.Procedures.ToList();
                if (procedureList.Count > 0)
                {
                    foreach (var item in procedureList)
                    {
                        if (item.Client.Person.Email == username)
                        {
                            casesToReturn.Add(new ProcedureModel {
                                ProcedureID = item.ProcedureID,
                                ProcedureStateID = item.ProcedureStateID,
                                ClientID = (int)item.ClientID,
                                FinishingDate = (DateTime)item.FinishingDate,
                                StartingDate = (DateTime)item.StartingDate,
                                LawyerID = item.LawyerID
                            });
                        }
                    }
                }
                else
                {
                    //FAKE PROCEDURE
                    casesToReturn.Add(new ProcedureModel
                    {
                        ProcedureID = -1
                    });
                }
            }
            return casesToReturn;
        }

        public IList<ClientModel> GetAllClients()
        {
            IList<ClientModel> clientListModel = new List<ClientModel>();

            if (db.Clients != null)
            {
                IList<Client> ClientList = db.Clients.ToList();
                if (ClientList.Count > 0)
                {
                    foreach (var item in ClientList)
                    {
                        if (item.ClientID != -1)
                        {
                            clientListModel.Add(new ClientModel
                            {
                                ClientID = item.ClientID,
                                PersonID = item.PersonID,
                                Birthday = (DateTime)item.Person.Birthday,
                                DocumentNumber = item.Person.DocumentNumber,
                                DocumentNumberTypeID = item.Person.DocumentNumberTypeID,
                                Email = item.Person.Email,
                                EntranceDate = (DateTime)item.Person.EntranceDate,
                                FullName = item.Person.FullName,
                                LocationID = item.Person.LocationID,
                                GenderID = (short)item.Person.GenderID,
                                ProfileImage = item.Person.ProfileImage
                            });
                        }
                    }
                }
                else
                {
                    ClientModel fakeClient = new ClientModel
                    {
                        ClientID = -1
                    };
                    clientListModel.Add(fakeClient);
                }
            }
            return clientListModel;
        }

        public ClientModel GetClientByID(int id)
        {
            Client clientFromDB = db.Clients.Find(id);
            ClientModel clientToReturn = new ClientModel
            {
                ClientID = clientFromDB.ClientID,
                PersonID = clientFromDB.PersonID,
                Birthday = (DateTime)clientFromDB.Person.Birthday,
                DocumentNumber = clientFromDB.Person.DocumentNumber,
                DocumentNumberTypeID = clientFromDB.Person.DocumentNumberTypeID,
                Email = clientFromDB.Person.Email,
                EntranceDate = (DateTime)clientFromDB.Person.EntranceDate,
                FullName = clientFromDB.Person.FullName,
                LocationID = clientFromDB.Person.LocationID,
                GenderID = (short)clientFromDB.Person.GenderID,
                ProfileImage = clientFromDB.Person.ProfileImage
            };
            return clientToReturn;
        }

        public int GetNextClientID()
        {
            try
            {
                db.Clients.SqlQuery("SELECT ClientID FROM Clients ORDER BY ClientID DESC LIMIT 1");
                var Clients = db.Clients.ToList();
                if (Clients.Count == 0)
                {
                    return 1;
                }
                return Clients.Last().ClientID + 1;
            }
            catch
            {
                return 1;
            }
        }

        public bool SendNotificationToLawyer(int id)
        {
            try
            {
                NotificationServices _notification = new NotificationServices();
                db.Notifications.Add(new Notification
                {
                    LawyerID = id,
                    NotificationID = _notification.GetNextNotificationId(),
                    Seen = false
                });
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
