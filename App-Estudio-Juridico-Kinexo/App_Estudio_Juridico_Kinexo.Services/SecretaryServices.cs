﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class SecretaryServices : ISecretary
    {
        private App_Estudio_Juridico_KinexoEntities1 db;

        public SecretaryServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(SecretaryModel secretary)
        {
            try
            {
                int id = GetNextSecretaryID();
                DateTime today = DateTime.Today;
                Person newPerson = new Person
                {
                    PersonID = id,
                    FullName = secretary.FullName,
                    Title = "Secretaria",
                    DocumentNumber = secretary.DocumentNumber,
                    DocumentNumberTypeID = (short)secretary.DocumentNumberTypeID,
                    Email = secretary.Email,
                    Birthday = secretary.Birthday,
                    LocationID = (short)secretary.LocationID,
                    EntranceDate = today,
                    GenderID = secretary.GenderID
                };
                db.People.Add(newPerson);
                Secretary newSecretary = new Secretary
                {
                    SecretaryID = secretary.SecretaryID,
                    PersonID = secretary.PersonID
                };
                db.Secretaries.Add(newSecretary);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Secretary delete = db.Secretaries.Find(id);
                db.Secretaries.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(int id, SecretaryModel secretary)
        {
            try
            {
                Secretary secretaryFound = db.Secretaries.Find(id);
                Person editPerson = db.People.Find(secretaryFound.PersonID);
                editPerson.LocationID = (short)secretary.LocationID;
                editPerson.Email = secretary.Email;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int GetNextSecretaryID()
        {
            if (db.Secretaries != null)
            {
                db.Secretaries.SqlQuery("SELECT SecretaryID FROM Secretaries ORDER BY SecretaryID DESC LIMIT 1");
                var Secretary = db.Secretaries.ToList();
                if (Secretary.Count == 0)
                {
                    return 1;
                }
                return Secretary.Last().SecretaryID + 1;
            }
            return 1;
        }

        public IList<SecretaryModel> GetSecretaries()
        {
            IList<SecretaryModel> secretaryListModel = null;
            if (db.Secretaries != null)
            {
                IList<Secretary> SecretaryList = db.Secretaries.ToList();
                foreach (var item in SecretaryList)
                {
                    secretaryListModel.Add(new SecretaryModel
                    {
                        SecretaryID = item.SecretaryID,
                        PersonID = item.PersonID,
                        FullName = item.Person.FullName,
                        Email = item.Person.Email,
                        EntranceDate = (DateTime)item.Person.EntranceDate,
                        Birthday = (DateTime)item.Person.Birthday,
                        GenderID = (int)item.Person.GenderID,
                        DocumentNumberTypeID = item.Person.DocumentNumberTypeID,
                        DocumentNumber = item.Person.DocumentNumber,
                        LocationID = item.Person.LocationID,
                        Title = item.Person.Title
                    });
                }
            }
            else
            {
                secretaryListModel.Add(new SecretaryModel
                {
                    SecretaryID = -1
                });
            }
            return secretaryListModel;
        }

        public SecretaryModel GetSecretaryByID(int id)
        {
            Secretary secretary = db.Secretaries.Find(id);
            SecretaryModel newSecretary = null;
            if (secretary != null)
            {
                newSecretary = new SecretaryModel
                {
                    PersonID = id,
                    FullName = secretary.Person.FullName,
                    Title = "Secretaria",
                    DocumentNumber = secretary.Person.DocumentNumber,
                    DocumentNumberTypeID = (short)secretary.Person.DocumentNumberTypeID,
                    Email = secretary.Person.Email,
                    Birthday = (DateTime)secretary.Person.Birthday,
                    LocationID = (short)secretary.Person.LocationID,
                    EntranceDate = (DateTime)secretary.Person.EntranceDate,
                    GenderID = (int)secretary.Person.GenderID
                };
            }
            return newSecretary;
        }
    }
}

