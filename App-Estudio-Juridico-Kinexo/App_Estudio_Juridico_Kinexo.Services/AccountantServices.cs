﻿using System;
using System.Collections.Generic;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;
using System.Linq;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class AccountantServices : IAccountant
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public AccountantServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(AccountantModel accountant)
        {
            try
            {
                DateTime today = DateTime.Today;
                if (accountant.ProfileImage == "")
                {
                    accountant.ProfileImage = "http://polyureashop.studio.crasman.fi/pub/web/img/no-image.jpg";
                }
                Person newPerson = new Person
                {
                    PersonID = accountant.PersonID,
                    FullName = accountant.FullName,
                    Title = accountant.Title,
                    DocumentNumber = accountant.DocumentNumber,
                    DocumentNumberTypeID = (short)accountant.DocumentNumberTypeID,
                    Email = accountant.Email,
                    Birthday = accountant.Birthday,
                    LocationID = (short)accountant.LocationID,
                    EntranceDate = today,
                    GenderID = accountant.GenderID,
                    ProfileImage = accountant.ProfileImage
                };
                db.People.Add(newPerson);
                Accountant newAccountant = new Accountant
                {
                    AccountantID = accountant.AccountantID,
                    PersonID = accountant.PersonID
                };
                db.Accountants.Add(newAccountant);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                Accountant delete = db.Accountants.Find(id);
                db.Accountants.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Edit(int id, AccountantModel accountant)
        {
            try
            {
                Accountant accountantFound = db.Accountants.Find(id);
                Person editPerson = db.People.Find(accountantFound.PersonID);
                editPerson.LocationID = (short)accountant.LocationID;
                editPerson.Email = accountant.Email;
                editPerson.ProfileImage = accountant.ProfileImage;
                db.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
        public IList<AccountantModel> GetAccountants()
        {
            IList<AccountantModel> accountantListModel = null;
            if (db.Accountants != null)
            {
                IList<Accountant> accountantList = db.Accountants.ToList();
                foreach (var item in accountantList)
                {
                    accountantListModel.Add(new AccountantModel
                    {
                        AccountantID = item.AccountantID,
                        PersonID = item.PersonID,
                        FullName = item.Person.FullName,
                        Email = item.Person.Email,
                        EntranceDate = (DateTime)item.Person.EntranceDate,
                        Birthday = (DateTime)item.Person.Birthday,
                        GenderID = (int)item.Person.GenderID,
                        DocumentNumberTypeID = item.Person.DocumentNumberTypeID,
                        DocumentNumber = item.Person.DocumentNumber,
                        LocationID = item.Person.LocationID,
                        Title = item.Person.Title,
                        ProfileImage = item.Person.ProfileImage
                    });
                }
            }
            else
            {
                accountantListModel.Add(new AccountantModel
                {
                    AccountantID = -1
                });
            }
            return accountantListModel;
        }
        public AccountantModel GetAccountantByID(int id)
        {
            Accountant found = db.Accountants.Find(id);
            AccountantModel accountant = null;
            if (found != null)
            {
                accountant = new AccountantModel
                {
                    AccountantID = found.AccountantID,
                    PersonID = found.PersonID,
                    Birthday = (DateTime)found.Person.Birthday,
                    DocumentNumber = found.Person.DocumentNumber,
                    DocumentNumberTypeID = found.Person.DocumentNumberTypeID,
                    Email = found.Person.Email,
                    EntranceDate = (DateTime)found.Person.EntranceDate,
                    FullName = found.Person.FullName,
                    GenderID = (int)found.Person.GenderID,
                    LocationID = found.Person.LocationID,
                    ProfileImage = found.Person.ProfileImage,
                    Title = found.Person.Title
                };
            }
            return accountant;
        }
        public int GetNextAccountantID()
        {
            if (db.Accountants != null)
            {
                db.Accountants.SqlQuery("SELECT AccountantID FROM Accountants ORDER BY AccountantID DESC LIMIT 1");
                var Contadores = db.Accountants.ToList();
                if (Contadores.Count == 0)
                {
                    return 1;
                }
                return Contadores.Last().AccountantID + 1;
            }
            return 1;
        }
    }
}
