﻿using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Data;
using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class NotificationServices : INotifications
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public NotificationServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }

        public int GetNextNotificationId()
        {
            if (db.Notifications != null)
            {
                db.Notifications.SqlQuery("SELECT NotificationID FROM Notifications ORDER BY NotificationID DESC LIMIT 1");
                var Notificaciones = db.Notifications.ToList();
                if (Notificaciones.Count == 0)
                {
                    return 1;
                }
                return Notificaciones.Last().NotificationID + 1;
            }
            return 1;
        }

        public bool SendNotificationToLawyer(int id)
        {
            try
            {
                db.Notifications.Add(new Notification
                {
                    NotificationID = GetNextNotificationId(),
                    LawyerID = id
                });
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        public IList<NotificationModel> GetAllNotificationsByLawyerID(int id)
        {
            try
            {
                List<NotificationModel> notificationsFound = new List<NotificationModel>();
                if (db.Notifications != null)
                {
                    IList<Notification> _notifications = db.Notifications.ToList();
                    if (_notifications.Count > 0)
                    {
                        foreach (var item in _notifications)
                        {
                            if (item.LawyerID == id)
                            {
                                notificationsFound.Add(new NotificationModel
                                {
                                    NotificationID = item.NotificationID,
                                    Seen = item.Seen,
                                    CommentID = item.CommentID,
                                    LawyerID = item.LawyerID
                                });
                            }
                        }
                    }
                    return notificationsFound;
                }
                return notificationsFound;
            }
            catch
            {
                List<NotificationModel> notNotificationsFound = new List<NotificationModel>();
                return notNotificationsFound;
            }
        }

        public IList<NotificationModel> GetAllNotificationsByUsername(string username)
        {
            try
            {
                var user = db.UserProfiles.First(u => u.UserName == username);
                var person = db.People.First(p => p.Email == user.UserName);
                Lawyer lawyer = db.Lawyers.First(l => l.PersonID == person.PersonID);
                List<NotificationModel> notificationsFound = new List<NotificationModel>();
                if (db.Notifications != null)
                {
                    IList<Notification> _notifications = db.Notifications.ToList();
                    if (_notifications.Count > 0)
                    {
                        foreach (var item in _notifications)
                        {
                            
                            if (item.LawyerID == lawyer.LawyerID)
                            {
                                notificationsFound.Add(new NotificationModel
                                {
                                    NotificationID = item.NotificationID,
                                    Seen = item.Seen,
                                    Comment = db.Comments.First(c => c.CommentID == item.CommentID).CommentDesc,
                                    Client = db.Clients.First(c => c.ClientID == item.ClientID).Person.FullName
                                });
                            }
                        }
                    }
                    else
                    {
                        notificationsFound.Add(new NotificationModel
                        {
                            NotificationID = -1
                        });
                    }
                    return notificationsFound;
                }
                return notificationsFound;
            }
            catch
            {
                List<NotificationModel> notNotificationsFound = new List<NotificationModel>();
                return notNotificationsFound;
            }
        }
    }
}
