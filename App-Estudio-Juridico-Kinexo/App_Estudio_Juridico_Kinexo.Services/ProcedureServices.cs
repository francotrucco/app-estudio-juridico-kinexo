﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class ProcedureServices : IProcedure
    {
        private App_Estudio_Juridico_KinexoEntities1 db;

        CommentServices commentSvs = new CommentServices();

        public ProcedureServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(ProcedureModel procedure)
        {
            try
            {
                if (procedure.Comments != null)
                {
                    foreach (var p in procedure.Comments)
                    {
                        commentSvs.CreateNewComment(new CommentModel
                        {
                            Comment = p.Comment,
                            CommentID = commentSvs.GetNextCommentId(),
                            ProcedureID = p.ProcedureID
                        });
                    }
                    
                }
                db.Procedures.Add(new Procedure
                {
                    LawyerID = procedure.LawyerID,
                    ProcedureID = GetNextProcedureID(),
                    ProcedureStateID = db.ProcedureStates.Find(1).ProcedureStateID,
                    ClientID = procedure.ClientID,
                    StartingDate = DateTime.Today,
                    FinishingDate = procedure.FinishingDate
                });
                db.SaveChanges();
                return true;
            }
            catch (NotImplementedException e)
            {
                return false;
            }
        }
        public IList<CommentModel> GetAllComentsFromProcedure(int id)
        {
            IList<CommentModel> commentsToReturn = new List<CommentModel>();

            if (db.Comments != null)
            {
                IList<Comment> commentsFromDB = db.Comments.ToList();
                Procedure procedureFromDB = db.Procedures.Find(id);
                if (commentsFromDB.Count > 1)
                {
                    foreach (var item in commentsFromDB)
                    {
                        if (procedureFromDB.ProcedureID == item.ProcedureID)
                        {
                            commentsToReturn.Add(new CommentModel
                            {
                                Comment = item.CommentDesc,
                                CommentID = item.CommentID,
                                ProcedureID = item.ProcedureID
                            });
                        }
                        else
                        {
                            //// If reached this way it's because there's no comment from that procedure
                            commentsToReturn.Add(new CommentModel
                            {
                                CommentID = -2
                            });
                        }
                    }
                }
                else
                {
                    //// Fake comment that indicates that there's no Comment storaged in the data base.
                    commentsToReturn.Add(new CommentModel
                    {
                        CommentID = -1
                    });
                }
            }
            return commentsToReturn;
        }

        public bool Delete(int id)
        {
            try
            {
                var procedureToRemove = db.Procedures.Find(id);
                db.Procedures.Remove(procedureToRemove);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(int id, ProcedureModel procedure)
        {
            try
            {
                Procedure procedureToEdit = db.Procedures.Find(id);
                procedureToEdit.LawyerID = procedure.LawyerID;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int GetNextProcedureID()
        {
            if (db.Procedures != null)
            {
                db.Procedures.SqlQuery("SELECT ProcedureID FROM Procedures ORDER BY ProcedureID DESC LIMIT 1");
                var procedures = db.Procedures.ToList();
                if (procedures.Count == 0)
                {
                    return 1;
                }
                return procedures.Last().ProcedureID + 1;
            }
            return 1;
        }

        public ProcedureModel GetProcedureByID(int id)
        {
            try
            {
                ProcedureModel procedureToReturn = new ProcedureModel();
                if (db.Procedures != null)
                {
                    Procedure foundProcedure = db.Procedures.Find(id);
                    procedureToReturn.ClientID = (int)foundProcedure.ClientID;
                    procedureToReturn.StartingDate = (DateTime)foundProcedure.StartingDate;
                    procedureToReturn.FinishingDate = (DateTime)foundProcedure.FinishingDate;
                    procedureToReturn.LawyerID = foundProcedure.LawyerID;
                    procedureToReturn.ProcedureID = foundProcedure.ProcedureID;
                    procedureToReturn.ProcedureStateID = foundProcedure.ProcedureStateID;
                }
                return procedureToReturn;
            }
            catch
            {
                ProcedureModel emptyProcedure = new ProcedureModel();
                return emptyProcedure;
            }
        }

        public bool GetToNextProcedureState(int id)
        {
            try
            {
                var procedure = db.Procedures.Find(id);
                if (procedure.ProcedureStateID == 5)
                {
                    return false;
                }
                procedure.ProcedureStateID++;
                db.SaveChanges();
                return true;
            }
            catch (NotImplementedException e)
            {
                return false;
            }
            

        }

        public IList<ProcedureModel> GetAllProceduresOfClient(int id)
        {
            IList<ProcedureModel> procedureListModel = new List<ProcedureModel>();

            if (db.News != null)
            {
                IList<Procedure> ProcedureList = db.Procedures.ToList();
                if (ProcedureList.Count > 0)
                {
                    foreach (var item in ProcedureList)
                    {
                        procedureListModel.Add(new ProcedureModel
                        {
                            ProcedureID = item.ProcedureID,
                            ProcedureStateID = item.ProcedureStateID,
                            ClientID = (int)item.ClientID,
                            FinishingDate = (DateTime)item.FinishingDate,
                            StartingDate = (DateTime)item.StartingDate,
                            LawyerID = item.LawyerID,
                            LawyerInCharge = db.Lawyers.First(l => l.LawyerID == item.LawyerID).Person.FullName
                        });
                    }
                }
                else
                {
                    ProcedureModel fakeProcedure = new ProcedureModel
                    {
                        ProcedureID = -1
                    };
                    procedureListModel.Add(fakeProcedure);
                }
            }

            return procedureListModel;
        }
    }
}
