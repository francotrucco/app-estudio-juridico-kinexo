﻿using System;
using System.Collections.Generic;
using System.Linq;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;
namespace App_Estudio_Juridico_Kinexo.Services
{
    public class HumanResourceManagerServices : IHumanResourceManager
    {
        private App_Estudio_Juridico_KinexoEntities1 db;

        public HumanResourceManagerServices()
        {
            this.db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(HumanResourceManagerModel HRM)
        {
            try
            {
                int id = db.People.Last().PersonID + 1;
                DateTime today = DateTime.Today;
                Person newPerson = new Person
                {
                    PersonID = id,
                    FullName = HRM.FullName,
                    Title = HRM.Title,
                    DocumentNumber = HRM.DocumentNumber,
                    DocumentNumberTypeID = (short)HRM.DocumentNumberTypeID,
                    Email = HRM.Email,
                    Birthday = HRM.Birthday,
                    LocationID = (short)HRM.LocationID,
                    EntranceDate = today,
                    GenderID = HRM.GenderID
                };
                db.People.Add(newPerson);
                HumanResourceManager newHRM = new HumanResourceManager
                {
                    HumanResourceManagerID = HRM.HumanResourceManagerID,
                    PersonID = HRM.PersonID
                };
                db.HumanResourceManagers.Add(newHRM);
                db.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                HumanResourceManager delete = db.HumanResourceManagers.Find(id);
                db.HumanResourceManagers.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(int id, HumanResourceManagerModel HRM)
        {
            try
            {
                Accountant accountantFound = db.Accountants.Find(id);
                Person editPerson = db.People.Find(accountantFound.PersonID);
                editPerson.LocationID = (short)HRM.LocationID;
                editPerson.Email = HRM.Email;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IList<HumanResourceManagerModel> GetHumanResourceManagers()
        {
            IList<HumanResourceManagerModel> HRMListModel = null;
            if (db.HumanResourceManagers != null)
            {
                IList<HumanResourceManager> HRMList = db.HumanResourceManagers.ToList();
                foreach (var item in HRMList)
                {
                    HRMListModel.Add(new HumanResourceManagerModel
                    {
                        HumanResourceManagerID = item.HumanResourceManagerID,
                        PersonID = item.PersonID,
                        FullName = item.Person.FullName,
                        Email = item.Person.Email,
                        EntranceDate = (DateTime)item.Person.EntranceDate,
                        Birthday = (DateTime)item.Person.Birthday,
                        DocumentNumberTypeID = item.Person.DocumentNumberTypeID,
                        DocumentNumber = item.Person.DocumentNumber,
                        LocationID = item.Person.LocationID,
                        Title = item.Person.Title
                    });
                }
            }
            else
            {
                HRMListModel.Add(new HumanResourceManagerModel
                {
                    HumanResourceManagerID = -1
                });
            }
            return HRMListModel;
        }

        public int GetNextHumanResourceManagerID()
        {
            if (db.HumanResourceManagers != null)
            {
                db.HumanResourceManagers.SqlQuery("SELECT HumanResourceManagerID FROM HumanResourceManagers ORDER BY HumanResourceManagersID DESC LIMIT 1");
                var HRMs = db.HumanResourceManagers.ToList();
                if (HRMs.Count == 0)
                {
                    return 1;
                }
                return HRMs.Last().HumanResourceManagerID + 1;
            }
            return 1;
        }

        public HumanResourceManagerModel GetHumanResourceManagerByID(int id)
        {
            HumanResourceManager HRM = db.HumanResourceManagers.Find(id);
            HumanResourceManagerModel HRMtoReturn = null;
            if (HRM != null)
            {
                HRMtoReturn = new HumanResourceManagerModel
                {
                    HumanResourceManagerID = HRM.HumanResourceManagerID,
                    Birthday = (DateTime)HRM.Person.Birthday,
                    DocumentNumber = HRM.Person.DocumentNumber,
                    DocumentNumberTypeID = HRM.Person.DocumentNumberTypeID,
                    Email = HRM.Person.Email,
                    EntranceDate = (DateTime)HRM.Person.EntranceDate,
                    FullName = HRM.Person.FullName,
                    LocationID = HRM.Person.LocationID,
                    PersonID = HRM.PersonID,
                    ProfileImage = HRM.Person.ProfileImage,
                    Title = HRM.Person.Title
                };
            }
            return HRMtoReturn;
        }
    }
}
