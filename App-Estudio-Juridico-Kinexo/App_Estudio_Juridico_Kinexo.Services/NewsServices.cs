﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class NewsServices : INews
    {
        private App_Estudio_Juridico_KinexoEntities1 db;

        public NewsServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool Create(NewsModel news)
        {
            try
            {
                if (news.Fonts == "")
                {
                    news.Fonts = "http://hdimagesnew.com/wp-content/uploads/2016/09/image-not-found.jpg";
                }
                News newTiding = new News
                {
                    NewsID = news.NewID,
                    Head = news.Head,
                    Body = news.Body,
                    Author = news.Author,
                    Fonts = news.Fonts,
                    ImageURL = news.ImageURL,
                    PublicationDate = DateTime.Today
                };
                db.News.Add(newTiding);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                News delete = db.News.Find(id);
                db.News.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(int id, NewsModel news)
        {
            try
            {
                News edit = db.News.Find(id);
                edit.Author = news.Author;
                edit.Body = news.Body;
                edit.Head = news.Head;
                edit.Fonts = news.Fonts;
                edit.ImageURL = news.ImageURL;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public NewsModel GetNewByID(int id)
        {
            News newFromDB = db.News.Find(id);
            NewsModel newToReturn = null;
            if (newFromDB != null)
            {
                newToReturn = new NewsModel
                {
                    NewID = newFromDB.NewsID,
                    Author = newFromDB.Author,
                    Body = newFromDB.Body,
                    Fonts = newFromDB.Fonts,
                    Head = newFromDB.Head,
                    ImageURL = newFromDB.ImageURL,
                    PublicationDate = (DateTime)newFromDB.PublicationDate
                };
            }
            return newToReturn;

        }

        public IList<NewsModel> GetNews()
        {
            IList<NewsModel> newsListModel = new List<NewsModel>();

            if (db.News != null)
            {
                IList<News> NewsList = db.News.ToList();
                if (NewsList.Count > 0)
                {
                    foreach (var item in NewsList)
                    {
                        newsListModel.Add(new NewsModel
                        {
                            Head = item.Head,
                            Body = item.Body,
                            Author = item.Author,
                            Fonts = item.Fonts,
                            ImageURL = item.ImageURL,
                            PublicationDate = (DateTime)item.PublicationDate,
                            NewID = item.NewsID
                        });
                    }
                }
                else
                {
                    NewsModel fakeNew = new NewsModel
                    {
                        NewID = -1
                    };
                    newsListModel.Add(fakeNew);
                }
            }

            return newsListModel;
        }

        public int GetNextNewID()
        {
            if (db.News != null)
            {
                db.News.SqlQuery("SELECT NewsID FROM News ORDER BY NewsID DESC LIMIT 1");
                var Noticias = db.News.ToList();
                if (Noticias.Count == 0)
                {
                    return 1;
                }
                return Noticias.Last().NewsID + 1;
            }
            return 1;
        }
    }
}
