﻿using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Data;
using App_Estudio_Juridico_Kinexo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class CommentServices : IComments
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public CommentServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public bool CreateNewComment(CommentModel comment)
        {
            try
            {
                db.Comments.Add(new Comment {
                    CommentDesc = comment.Comment,
                    CommentID = GetNextCommentId(),
                    ProcedureID = comment.ProcedureID
                });
                return true;
            }
            catch (NotImplementedException e)
            {
                return false;
            }
        }

        public CommentModel GetCommentById(int id)
        {
            CommentModel commentToReturn = new CommentModel();
            if (db.Comments != null)
            {
                Comment commentFromDB = db.Comments.Find(id);
                if (commentFromDB == null)
                {
                    commentFromDB = db.Comments.FirstOrDefault();
                    commentToReturn.CommentID = commentFromDB.CommentID;
                    commentToReturn.Comment = commentFromDB.CommentDesc;
                    commentToReturn.ProcedureID = commentFromDB.ProcedureID;
                }
                else
                {
                    commentToReturn.Comment = commentFromDB.CommentDesc;
                    commentToReturn.CommentID = commentFromDB.CommentID;
                    commentToReturn.ProcedureID = commentFromDB.ProcedureID;
                }
            }
            else
            {
                commentToReturn.CommentID = -1;
            }
            return commentToReturn;
        }

        public int GetNextCommentId()
        {
            try
            {
                List<Comment> commentList = db.Comments.ToList();
                return commentList.Last().CommentID + 1;
            }
            catch (NotImplementedException e)
            {
                return -1;
            }
        }
    }
}
