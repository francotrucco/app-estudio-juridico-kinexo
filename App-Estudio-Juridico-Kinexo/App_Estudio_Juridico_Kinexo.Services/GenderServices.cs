﻿using App_Estudio_Juridico_Kinexo.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class GenderServices : IGender
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public GenderServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public IList<GenderModel> GetAllGenders()
        {
            List<GenderModel> genders = new List<GenderModel>();
            var gen = db.Genders;
            foreach (var item in gen)
            {
                genders.Add(new GenderModel
                {
                    GenderID = item.GenderID,
                    Gender = item.Gender1
                });
            }
            return genders;
        }

        public GenderModel GetFirstGender()
        {
            Gender gen = db.Genders.Find(1);
            return new GenderModel
            {
                Gender = gen.Gender1,
                GenderID = gen.GenderID
            };
        }
    }
}
