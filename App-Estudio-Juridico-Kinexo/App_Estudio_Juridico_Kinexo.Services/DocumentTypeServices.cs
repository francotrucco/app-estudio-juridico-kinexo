﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Models;
using App_Estudio_Juridico_Kinexo.Data;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class DocumentTypeServices : IDocumentTypeNumber
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public DocumentTypeServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public IList<DocumentTypeNumberModel> GetDocumentNumberTypes()
        {
            List<DocumentTypeNumberModel> TiposDNI = new List<DocumentTypeNumberModel>();
            var tiposDNIEnBDD = db.DocumentNumberTypes;
            if (tiposDNIEnBDD != null)
            {
                foreach (var item in tiposDNIEnBDD)
                {
                    TiposDNI.Add(new DocumentTypeNumberModel
                    {
                        DocumentNumberTypeID = item.DocumentNumberTypeID,
                        DocumentType = item.DocumentNumberType1,
                        Abbreviation = item.Abbreviation
                    });
                }
            }
            return TiposDNI;
        }

        public DocumentTypeNumberModel GetFirstDocumentNumberType()
        {
            DocumentNumberType DNT = db.DocumentNumberTypes.Find(1);
            return new DocumentTypeNumberModel
            {
                DocumentNumberTypeID = DNT.DocumentNumberTypeID,
                DocumentType = DNT.DocumentNumberType1,
                Abbreviation = DNT.Abbreviation
            };
        }
    }
}
