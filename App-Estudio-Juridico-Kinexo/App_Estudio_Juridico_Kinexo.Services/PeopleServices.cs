﻿using App_Estudio_Juridico_Kinexo.Contracts;
using App_Estudio_Juridico_Kinexo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Estudio_Juridico_Kinexo.Services
{
    public class PeopleServices : IPeople
    {
        private App_Estudio_Juridico_KinexoEntities1 db;
        public PeopleServices()
        {
            db = new App_Estudio_Juridico_KinexoEntities1();
        }
        public int GetNextPeopleId()
        {
            if (db.People != null)
            {
                db.People.SqlQuery("SELECT PersonID FROM People ORDER BY PersonID DESC LIMIT 1");
                var Personas = db.People.ToList();
                if (Personas.Count == 0)
                {
                    return 1;
                }
                return Personas.Last().PersonID + 1;
            }
            return 1;
        }
    }
}
