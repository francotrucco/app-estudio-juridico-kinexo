//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App_Estudio_Juridico_Kinexo.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class LawyerSpecialty
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LawyerSpecialty()
        {
            this.Lawyers = new HashSet<Lawyer>();
        }
    
        public int LawyerSpecialtyID { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lawyer> Lawyers { get; set; }
    }
}
